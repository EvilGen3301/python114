# GUI - Graphical User Interface
# CLI - Command Line Interface (Terminal, Shell, Console)
print('Hello World')


# Arithmetic Operators
# Integers - მთელი რიცხვები
print(5 + 3)
print(5 - 3)
print(5 * 3)
print(15 / 3)
# integer division
print(15 // 3)
# modulus
print(15 % 3)
print(15 % 4)
# exponent
print(5 ** 3)
print(5 ** 2)

# Floats - წილადი რიცხვები
print(5.1 + 3.4)
print(0.1 + 0.3)
print(0.1 + 0.2)
print(round(0.1 + 0.2, 2))
print((10 + 20) / 100)
