name = 'Johnatan'
heading = 'This book is amazing'

print(type(name))
print(name)

print(name + ' is a student')
print(name * 3)
# Accessing character
print(name[0])
print(name[1])
print(name[2])
print(name[3])
print(len(name))
print(name[len(name) // 2])
print('last character is:', name[len(name) - 1])
print('last character is:', name[-1])

print(name.upper())
print(name.lower())
print(name.capitalize())
print(f'{heading.capitalize() = }')
print(heading.lower())
print(f'{heading.title() = }')
