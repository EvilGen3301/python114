# Declare a variable
# = - assignment operator
# right side is a value (data)
# left side is a variable (memory location)
name = 'John'  # string (str)
age = 25  # integer (int) -Inf, ..., -2, -1, 0, 1, 2, ..., Inf
height = 1.85  # float (float) -Inf, ..., -2.5, -2.4, ..., 0, 0.1, ..., Inf

# Concatenation
print('Hello' + ' ' + name + ' I am ' + age + ' years old')
