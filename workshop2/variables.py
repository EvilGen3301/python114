# naming a variable
# 1. start with a letter [a-z] or underscore _
# 2. followed by letters, digits, or underscores
# 3. case-sensitive
# 4. no reserved words
# 5. descriptive
# 6. snake_case (no camelCase or PascalCase)
# 7. no whitespace
# 8. no special characters (!@#$%^&*()-+)

l = 5
a = 3

# bad
fn = 'Nolan'
# better
first_name = 'Nolan'

firstName = 'Luka'
first_name = 'Luka'
