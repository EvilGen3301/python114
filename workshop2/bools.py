is_raining = True # False
is_windy = False

went_to_cinema = True
ate_popcorn = True

print(type(is_raining))
print(is_raining)

# AND Truth table
print(went_to_cinema and ate_popcorn)
print('AND Truth Table')
print('True and True:', True and True)
print('True and False:', True and False)
print('False and True', False and True)
print('False and False', False and False)

# OR Truth Table
print('OR Truth Table')
print('True or True:', True or True)
print('True or False:', True or False)
print('False or True:', False or True)
print('False or False:', False or False)

# NOT
print('NOT Truth Table')
print('not True:', not True)
print('not False:', not False)
